# Olug Oppløsning

Det er foreslått å formelt oppløse OLUG siden det ikke har vært aktivitet siden 2011. Ikke engang genfors har vært gjennomført slik vedtektene krever. Oppløsning krever 2 ekstraoridinære generalforsamlinger med minst 3 uker mellom. 

## Fremdrift 
* Styreleder verifiserer styrets kontaktinfo. Samtidig linkes til dette dokumentet. Kontaktinfo hentes fra genfors2011-referat. 
* Første genfors
  * Første innkalling godkjennes av hele styret før den sendes ut.
  * Innkalling sendes mer enn 2 uker før genfors
  * Genfors gjennomføres på https://meet.redpill-linpro.com/OLUG
* Andre genfors
  * Andre innkalling godkjennes av hele styret før den sendes ut.
  * Innkalling sendes mer enn 2 uker før genfors
  * Genfors gjennomføres på https://meet.redpill-linpro.com/OLUG
* Avhendig sluttføres

## Avhending av foreningens ressurser

* Rett til bruk av foreningens navn, domene og epostlister opphører samtidig med foreningens oppløsning. Derfor er det greit å avklare hvordan . 
* Olugs epostliste brukes til å invitere alle medlemmene til oplug og nuugs epostlister. Etter at invitasjonene er sendt destrueres olugs-epostarkiv. 
* Nettsiden dupliseres til statisk html og lagres på egnet netthosting. Styreleder gjør sitt beste for at dette er tilgjengelig minst 1 år etter oppløsning. Ditto med domenet olug.no. Dette i tilfelle noen ønsker å plukke opp restene og fortsette drift av foreningen under samme navn. Om noen ønsker å ta i bruk foreningens navn (og domenenavn) skal styreleder overdra domene til nytt styre om ny virksomhet har et nærliggende formål som OLUG hadde. 
* Annet?

## Forslag til innkalling

Publiseres på: 

* OLUGs egen epostliste
* NUUGs epostliste
* no.it.os.unix.linux.diverse
* Flere forslag mottas med takk. Er det f.eks relevante Reddit-grupper?

**OLUG Ekstraoridinær generalforsamling **

Det innkalles med dette til ekstraoridinær generalforsamling i foreningen OLUG. Følgende saker skal behandles:

* Innkalling
* Oppløsning av foreningen

Generalforsamlingen avholdes på videokonferanse[1] av hensyn til smittevern under pågående pandemi. Så langt det er mulig følger vi foretaklovgivingsens midlertidlige lov om digitale møter [2]. Dette krever blant annet at styret er enstemmig i vedtaket om å bruke digitale møter.

Det vises til https://gitlab.com/perhenrik/olug-opplosning for utfyllende detaljer rundt oppløsningen.

Vennlig hilsen
Per Henrik Nystrøm (tidl Oja) på vegne av styret i OLUG.

1: https://meet.redpill-linpro.com/OLUG
2: Midlertidig lov om unntak fra krav til fysisk møte mv. i foretakslovgivningen for å avhjelpe konsekvenser av utbrudd av covid-19 (LOV-2020-05-26-54) https://lovdata.no/dokument/NL/lov/2020-05-26-54